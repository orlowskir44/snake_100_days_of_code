from turtle import Turtle

ALIGN = 'center'
FONT = ('Courier', 18, 'normal')


class Scoreboard(Turtle):

    def __init__(self):
        super().__init__()
        self.score = 0
        with open('data.txt') as file:
            self.high_score = int(file.read())
        self.color('white')
        self.hideturtle()
        self.penup()
        self.goto(0, 280)
        self.update_sc()

    def increase(self):
            self.score +=1
            self.update_sc()
    def update_sc(self):
        self.clear()
        self.write(f'Score: {self.score} High Score: {self.high_score}', False, align=ALIGN, font=FONT)

    def reset(self):
        if self.high_score < self.score:
            self.high_score = self.score
            with open('data.txt', mode='w') as file:
                file.write(f'{self.high_score}')
        self.score = 0
        self.update_sc()

    def save_highscore(self, hs):
        with open('data.txt', mode='w') as f:
            hs = str(hs)
            f.write(hs)
            f.close()


    # def game_over(self):
    #     self.goto(0, 0)
    #     self.write("GAME OVER", False, align=ALIGN, font=FONT)
