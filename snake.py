from turtle import Turtle

starting_position = [(0, 0), (-20, 0), (-40, 0)]
MOVE = 20

UP = 90
DOWN = 270
LEFT = 180
RIGHT = 0

class Snake:

    def __init__(self):
      self.segments = []
      self.create_snake()
      self.head = self.segments[0]

    def create_snake(self):
        for position in starting_position:
            self.add_segment(position)

    def add_segment(self, position):
        new_seg = Turtle()
        new_seg.shape("square")
        new_seg.color("white")
        # new_seg.speed(0)
        new_seg.penup()
        new_seg.goto(position)
        self.segments.append(new_seg)

    def reset(self):
        for seg in self.segments:
            seg.reset()
        self.segments.clear()
        self.create_snake()
        self.head = self.segments[0]

    def extend(self):
        self.add_segment(self.segments[-1].position())

    def move(self):
        for r_seg in range(len(self.segments) - 1, 0, -1):
            new_x = self.segments[r_seg - 1].xcor()
            new_y = self.segments[r_seg - 1].ycor()
            self.segments[r_seg].goto(x=new_x, y=new_y)
        self.head.forward(MOVE)

    def up(self):
        if self.head.heading() != DOWN:
            self.head.setheading(UP)

    def down(self):
        if self.head.heading() != UP:
            self.head.setheading(DOWN)

    def left(self):
        if self.head.heading() != RIGHT:
            self.head.setheading(LEFT)

    def right(self):
        if self.head.heading() != LEFT:
            self.head.setheading(RIGHT)
